View the original README from the [City of Chicago](https://chicago.github.io/climatechangeisreal/). 

# About this version

The City of Philadelphia is providing a forked version of Chicago's repository, ready to be hosted on GitHub with minimal configuration. This version supports the folder structure that gh-pages requires. 

## How to host this content

1. Fork or clone this project to your organization's repository. 
2. In the repository settings, choose the `master` branch as the GitHub pages source.
3. Change the content `branding` directory to suit your needs.

Note: This version of the website is dependent on the repository name remaining `climatechangeisreal`. 
